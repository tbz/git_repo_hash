# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

# Source: https://gitlab.mpcdf.mpg.de/tbz/git_repo_hash
# Copyright 2022-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)

# this makefile sets the value for the following 3 variables:
#   git commit hash
GIT_HASH = unknown
#   status of git repository (modified / unmodified / unknown)
GIT_STATUS = unknown
#   source of information (git / internal git parser / CI environment)
GIT_INFO_SRC = N/A


# "git" command exists?
GIT_TOOL_EXISTS = $(if $(shell command -v git 2> /dev/null),1,0)

ifeq ($(GIT_TOOL_EXISTS),1)
	GIT_STATUS_RETURN = $(shell git branch -r 1>/dev/null 2>&1 && echo "0")
	ifeq ($(GIT_STATUS_RETURN),0)             #repo info exists
		GIT_STATUS_LINES = $(shell git status --short -uno | wc -l)
		ifeq ($(GIT_STATUS_LINES),0)
			GIT_STATUS = unmodified
		else
			GIT_STATUS = modified
		endif
		GIT_HASH = $(shell git rev-parse HEAD)
	endif
	GIT_INFO_SRC = git
else
	ifneq ("$(wildcard .git/HEAD)","")                  		#git/HEAD exists
		ifneq ("$(findstring ref:,$(shell cat .git/HEAD))","")	# HEAD is ref:
			BRANCH_REF = $(subst ref: ,,$(shell cat .git/HEAD))
			ifneq ("$(wildcard .git/$(BRANCH_REF))","")         #BRANCH info exists
				GIT_HASH = $(shell cat .git/$(BRANCH_REF))
			endif
		else
			GIT_HASH = $(shell cat .git/HEAD)
		endif
			GIT_INFO_SRC = internal git parser
	endif
endif

ifeq ($(GIT_HASH),unknown)
	# supported by GitLab-CI and Woodpecker-CI
	ifdef CI_COMMIT_SHA
		GIT_HASH = $(CI_COMMIT_SHA)
		GIT_INFO_SRC = CI environment
	endif
endif
