# git repository hash

A makefile for extracting commit hash of source git repository.

`git_repo_hash.mk` sets the following make variables on execution:

- `GIT_HASH`: git commit hash
- `GIT_STATUS`: status of the git repository (modified/unmodified/unknown)
- `GIT_INFO_SRC`: source of this information (git/internal git parser/CI environment)

## usage

0. Add (copy) `include/git_repo_hash.mk` to your project.
1. Include `git_repo_hash.mk` in your makefile.

```makefile
include git_repo_hash.mk
```

2. Add a make target to write the generated makefile variables to a file. For an example, see `git_repo_hash.h` target in `makefile`.
3. Include the generated file in your code. For an example, see `git_repo_hash_print.c`.

## requirements

- make
- cat, git (optional)

## license and attributions

Copyright 2022-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)
All rights reserved.

This software may be modified and distributed under the terms of the GNU Lesser General Public License 3.0. See the LICENSE file for details.
