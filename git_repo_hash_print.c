#include "git_repo_hash.h"
#include <stdio.h>

int main(int argc, char *argv[]) {

  printf("git hash               : %s\n", git_hash);
  printf("git status             : %s\n", git_status);
  printf("git information source : %s\n", git_info_src);

  return 0;
}