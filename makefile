.POSIX:
include include/git_repo_hash.mk

GIT_HASH_DEF = "const char *git_hash=\"$(GIT_HASH)\";"
GIT_STATUS_DEF = "const char *git_status=\"$(GIT_STATUS)\";"
GIT_INFO_SRC_DEF = "const char *git_info_src=\"$(GIT_INFO_SRC)\";"
GIT_INFO = $(GIT_HASH_DEF)'\n'$(GIT_STATUS_DEF)'\n'$(GIT_INFO_SRC_DEF)

.PHONY: all force clean

all: git_repo_hash_print

git_repo_hash_print: git_repo_hash.h

git_repo_hash.h: force
	@echo -e $(GIT_INFO) | cmp -s - $@ || echo -e $(GIT_INFO) > $@

clean:
	rm -f git_repo_hash.h git_repo_hash_print
